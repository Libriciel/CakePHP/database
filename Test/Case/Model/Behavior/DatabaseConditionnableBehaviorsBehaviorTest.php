<?php
/**
 * Code source de la classe DatabaseConditionnableBehaviorsBehaviorTest.
 *
 * PHP 5.3
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('Model', 'Model');
App::uses('AppModel', 'Model');

require_once CakePlugin::path('Database') . 'Config' . DS . 'bootstrap.php';

/**
 * La classe DatabaseConditionnableBehaviorsBehaviorTest effectue les tests unitaires
 * de la classe DatabaseConditionnableBehaviorsBehavior.
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 */
class DatabaseConditionnableBehaviorsBehaviorTest extends CakeTestCase
{

    /**
     * Fixtures associated with this test case
     *
     * @var array
     */
    public $fixtures = ['plugin.Database.DatabaseSite'];

    /**
     * Method executed before each test
     */
    public function setUp()
    {
        parent::setUp();

        ClassRegistry::flush();
        Cache::clear();

        $this->Site = ClassRegistry::init(['class' => 'Database.DatabaseSite', 'alias' => 'Site']);
        foreach ($this->Site->Behaviors->loaded() as $behavior) {
            $this->Site->Behaviors->unload($behavior);
        }
    }

    /**
     * Method executed after each test
     */
    public function tearDown()
    {
        unset($this->Site);
        parent::tearDown();
    }

    public function assertLoaded(Model $Model, array $expected)
    {
        $actual = $Model->Behaviors->loaded();
        sort($actual);
        sort($expected);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    /**
     * Test de la méthode DatabaseConditionnableBehaviorsBehavior::setup() du plugin
     * Database.
     *
     * @return void
     */
    public function testSetupEmpty()
    {
        $this->Site->Behaviors->attach('Database.DatabaseConditionnableBehaviors');
        $expected = ['DatabaseConditionnableBehaviors'];
        $this->assertLoaded($this->Site, $expected);
    }

    /**
     * Test de la méthode DatabaseConditionnableBehaviorsBehavior::setup() du plugin
     * Database.
     *
     * @return void
     */
    public function testSetup()
    {
        $this->assertEquals([], $this->Site->Behaviors->loaded(), var_export($this->Site->Behaviors->loaded(), true));

        $config = [
            '$useTable !== false' => [
                'Containable'
            ],
            '$useTable === \'database_sites\'' => [
                'Database.DatabaseAutovalidate'
            ],
            'strpos($useTable, "database_") === 0' => [
                'Database.DatabaseEnumerable'
            ],
            sprintf('$dataSource === "%s"', get_class($this->Site->getDataSource())) => [
                'Database.DatabaseFormattable'
            ],
            '$name === "DatabaseSite" && $alias === "Site"' => [
                'Database.DatabaseTable' 
            ]
        ];
        $this->Site->Behaviors->attach('Database.DatabaseConditionnableBehaviors', $config);
        $expected = [
            'Containable',
            'DatabaseAutovalidate',
            'DatabaseConditionnableBehaviors',
            'DatabaseEnumerable',
            'DatabaseFormattable',
            'DatabaseTable'
        ];
        $this->assertLoaded($this->Site, $expected);
    }

    /**
     * Test de la méthode DatabaseConditionnableBehaviorsBehavior::setup() du plugin
     * Database avec lecture de la configuration.
     *
     * @return void
     */
    public function testSetupConfigure()
    {
        $this->assertEquals([], $this->Site->Behaviors->loaded(), var_export($this->Site->Behaviors->loaded(), true));

        $config = [
            '$configure["Security"]["salt"] === "'.Configure::read('Security.salt').'"' => [
                'Containable'
            ]
        ];
        $this->Site->Behaviors->attach('Database.DatabaseConditionnableBehaviors', $config);
        $expected = [
            'Containable',
            'DatabaseConditionnableBehaviors'
        ];
        $this->assertLoaded($this->Site, $expected);
    }

    /**
     * Test de la méthode DatabaseConditionnableBehaviorsBehavior::setup() du plugin
     * Database avec une erreur.
     *
     * @expectedException PHPUnit_Framework_Error_Notice
     * @expectedExceptionMessage Undefined variable: foo
     * @expectedExceptionCode E_NOTICE
     */
    public function testSetupWithError()
    {
        $config = [
            '$foo === $bar' => [
                'Database.DatabaseTable'
            ]
        ];
        $this->Site->Behaviors->attach('Database.DatabaseConditionnableBehaviors', $config);
    }

}
