<?php
App::uses( 'DatabaseLinkedRecordsBehavior', 'Database.Model/Behavior' );
require_once CakePlugin::path( 'Database' ).DS.'Test'.DS.'Case'.DS.'blog_models.php';

class DatabaseLinkedRecordsBehaviorTest extends CakeTestCase
{
    /**
     *
     * @var AppModel
     */
    public $Post = null;

    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.User',
        'core.Post',
        'core.Comment',
        'core.Author',
        'core.Tag',
        'core.PostsTag'
    ];

    protected function _attach(Model $Model)
    {
        $attachedAliases = array_unique(Hash::extract($Model->Behaviors->methods(), '{s}.0'));
        foreach ($attachedAliases as $attachedAlias) {
            $Model->Behaviors->detach($attachedAlias);
        }

        $Model->Behaviors->attach('Database.DatabaseLinkedRecords');
        $Model->Behaviors->attach('Database.DatabaseTable');
    }

    /**
     * Préparation du test.
     *
     * INFO: ne pas utiliser parent::setUp();
     */
    public function setUp()
    {
        $this->Post = ClassRegistry::init(
            [
                'class' => 'TestPost',
                'alias' => 'Post'
            ]
        );

        // On attache le bon behavior
        $Models = [
            $this->Post,
            $this->Post->Author,
            $this->Post->Comment,
            $this->Post->TestPostTag,
            $this->Post->Tag
        ];
        foreach ($Models as $Model) {
            $this->_attach($Model);
        }
    }

    /**
     * Nettoyage postérieur au test.
     */
    public function tearDown()
    {
        unset($this->Post);
        ClassRegistry::flush();
        parent::tearDown();
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsVirtualFieldName()
     */
    public function testLinkedRecordsVirtualFieldName()
    {
        $result = $this->Post->linkedRecordsVirtualFieldName('Comment');
        $expected = 'has_comment';
        $this->assertEquals( $expected, $result );
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsVirtualField()
     */
    public function testLinkedRecordsVirtualField()
    {
        // Sans argument particulier
        $result = $this->Post->linkedRecordsVirtualField('Comment');
        $expected = 'EXISTS(SELECT * FROM "public"."comments" AS "comments"   WHERE "comments"."post_id" = "Post"."id"    LIMIT 1)';
        $this->assertEquals($expected, $result);

        // Avec une condition supplémentaire
        $querydata = [
            'conditions' => [
                'Comment.id' => 5
            ]
        ];
        $result = $this->Post->linkedRecordsVirtualField('Comment', $querydata);
        $expected = 'EXISTS(SELECT * FROM "public"."comments" AS "comments"   WHERE "comments"."id" = 5 AND "comments"."post_id" = "Post"."id"    LIMIT 1)';
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsVirtualField()
     * avec des jointures.
     */
    public function testLinkedRecordsVirtualFieldWithJoins()
    {
        $querydata = [
            'joins' => [
                $this->Post->join('Comment', ['type' => 'INNER'])
            ],
            'conditions' => ['Post.title' => 'An amazing title']
        ];
        $result = $this->Post->Author->linkedRecordsVirtualField('Post', $querydata);
        $expected = 'EXISTS(SELECT * FROM "public"."posts" AS "posts" INNER JOIN "public"."comments" AS "comments" ON ("comments"."post_id" = "posts"."id")  WHERE "posts"."title" = \'An amazing title\' AND "posts"."author_id" = "Author"."id"    LIMIT 1)';
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsModelsAliases().
     */
    public function testLinkedRecordsModelsAliases()
    {
        // Relations hasAndBelongsToMany et hasMany
        $result = $this->Post->linkedRecordsModelsAliases();
        $expected = ['TestPostTag', 'Comment'];
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsCompleteQuerydata()
     */
    public function testDatabaseLinkedRecordsCompleteQuerydata()
    {
        $querydata = [
            'fields' => [
                'Post.id',
                'Post.name'
            ],
            'conditions' => [
                'Post.id >' => 10
            ],
            'order' => ['Post.id DESC']
        ];
        $result = $this->Post->linkedRecordsCompleteQuerydata($querydata, true);
        $expected = [
            'fields' => [
                'Post.id',
                'Post.name',
                '(EXISTS(SELECT * FROM "public"."posts_tags" AS "test_post_tags"   WHERE "test_post_tags"."post_id" = "Post"."id"    LIMIT 1) OR EXISTS(SELECT * FROM "public"."comments" AS "comments"   WHERE "comments"."post_id" = "Post"."id"    LIMIT 1)) AS "Post__has_linked_records"'
            ],
            'conditions' => [
                'Post.id >' => 10
            ],
            'order' => ['Post.id DESC']
        ];
        $this->assertEquals( $expected, $result );
    }

    /**
     * Test de la méthode DatabaseLinkedRecordsBehavior::linkedRecordsLoadVirtualFields()
     */
    public function testDatabaseLinkedRecordsLoadVirtualFields()
    {
        // 1. Pour un modèle lié, sans rien de particulier
        $this->Post->Author->virtualFields = [];
        $this->Post->Author->linkedRecordsLoadVirtualFields('Post');
        $expected = [
            'has_post' => 'EXISTS(SELECT * FROM "public"."posts" AS "posts"   WHERE "posts"."author_id" = "Author"."id"    LIMIT 1)'
        ];
        $this->assertEquals($expected, $this->Post->Author->virtualFields, var_export($this->Post->Author->virtualFields, true));

        // 2. Pour un modèle lié, avec une condition et une jointure
        $this->Post->Author->virtualFields = [];
        $querydata = [
            'contain' => false,
            'joins' => [
                $this->Post->join('Comment', ['type' => 'INNER'])
            ],
            'conditions' => ['Post.id' => 666]
        ];
        $this->Post->Author->linkedRecordsLoadVirtualFields(['Post' => $querydata]);
        $expected = [
            'has_post' => 'EXISTS(SELECT * FROM "public"."posts" AS "posts" INNER JOIN "public"."comments" AS "comments" ON ("comments"."post_id" = "posts"."id")  WHERE "posts"."id" = 666 AND "posts"."author_id" = "Author"."id"    LIMIT 1)'
        ];
        $this->assertEquals($expected, $this->Post->Author->virtualFields, var_export($this->Post->Author->virtualFields, true));
    }
}
