<?php

/**
 * Code source de la classe DatabaseEnumerableBehaviorTest.
 *
 * PHP 5.3
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('Model', 'Model');
App::uses('AppModel', 'Model');

require_once CakePlugin::path('Database') . 'Config' . DS . 'bootstrap.php';

/**
 * La classe DatabaseEnumerableBehaviorTest effectue les tests unitaires de
 * la classe DatabaseEnumerableBehavior.
 * 
 * @covers DatabaseEnumerableBehavior
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 */
class DatabaseEnumerableBehaviorTest extends CakeTestCase {

    /**
     * Fixtures associated with this test case
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Database.DatabaseSite'
    ];

    /**
     * Method executed before each test
     */
    public function setUp()
    {
        parent::setUp();

        ClassRegistry::flush();
        Cache::clear();

        setLocale(LC_ALL, 'fra');
        Configure::write('Config.language', 'fra');
        App::build(
            [
                'Locale' => [
                    CakePlugin::path('Database') . 'Test' . DS . 'test_app' . DS . 'Locale' . DS
                ]
            ],
            App::RESET
        );

        $this->Site = ClassRegistry::init(['class' => 'Database.DatabaseSite', 'alias' => 'Site']);
        $this->Site->Behaviors->detach('Database.DatabaseEnumerable');
        $this->Site->validate = [
            'enum_field_1' => [
                'inList' => [
                    'rule' => ['inList', ['foo', 'bar', 'baz', 'boz']]
                ]
            ],
            'enum_field_2' => [
                [
                    'rule' => ['inList', ['toto', 'tata', 'titi']]
                ]
            ]
        ];
    }

    /**
     * Method executed after each test
     */
    public function tearDown()
    {
        unset($this->Site);
        parent::tearDown();
    }

    /**
     * Test de la méthode DatabaseEnumerableBehavior::enums() du plugin
     * Database.
     *
     * @return void
     */
    public function testEnums()
    {
        $this->Site->Behaviors->attach('Database.DatabaseEnumerable');
        $result = $this->Site->enums();
        $expected = [
            'Site' => [
                'enum_field_1' => [
                    'foo' => 'Intitulé de "foo"',
                    'bar' => 'Intitulé de "bar"',
                    'baz' => 'Intitulé de "baz"',
                    'boz' => 'Intitulé de "boz"'
                ],
                'enum_field_2' => [
                    'toto' => 'Intitulé de "toto"',
                    'tata' => 'Intitulé de "tata"',
                    'titi' => 'Intitulé de "titi"'
                ]
            ]
        ];

        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode DatabaseEnumerableBehavior::enum() du plugin
     * Database.
     *
     * @return void
     */
    public function testEnum()
    {
        $this->Site->Behaviors->attach('Database.DatabaseEnumerable');
        $result = $this->Site->enum('enum_field_1');
        $expected = [
            'foo' => 'Intitulé de "foo"',
            'bar' => 'Intitulé de "bar"',
            'baz' => 'Intitulé de "baz"',
            'boz' => 'Intitulé de "boz"'
        ];

        $this->assertEquals($expected, $result, var_export($result, true));
    }
}
