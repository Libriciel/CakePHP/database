<?php
/**
 * Code source de la classe DatabaseFormattableBehaviorTest.
 *
 * PHP 5.3
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('Model', 'Model');
App::uses('AppModel', 'Model');

/**
 * La classe DatabaseFormattableBehaviorTest effectue les tests unitaires de
 * la classe DatabaseFormattableBehavior.
 *
 * @package Database
 * @subpackage Test.Case.Model.Behavior
 */
class DatabaseFormattableBehaviorTest extends CakeTestCase
{
    /**
     * Fixtures associated with this test case
     *
     * @var array
     */
    public $fixtures = array(
        'plugin.Database.DatabaseSite',
        'plugin.Database.DatabaseUser',
    );

    /**
     * Jeu de données et de résultats attendus.
     *
     * @var array
     */
    public $records = array(
        array(
            'data' => array(
                'Site' => array(
                    'name' => ' X   ',
                    'price' => ' '
                )
            ),
            'expected' => array(
                'Site' => array(
                    'name' => 'X',
                    'price' => null
                )
            )
        ),
        array(
            'data' => array(
                'Site' => array(
                    'id' => null,
                    'name' => ' X   ',
                    'user_id' => '255_25',
                    'price' => ' 6 666,987 ',
                    'published' => null,
                    'description' => null,
                    'birthday' => ' ',
                    'birthtime' => ' ',
                    'document' => ' XXX '
                )
            ),
            'expected' => array(
                'Site' => array(
                    'id' => null,
                    'name' => 'X',
                    'user_id' => '25',
                    'price' => '6666.987',
                    'published' => null,
                    'description' => null,
                    'birthday' => null,
                    'birthtime' => null,
                    'document' => ' XXX '
                )
            )
        ),
    );

    /**
     * Method executed before each test
     */
    public function setUp()
    {
        parent::setUp();

        ClassRegistry::flush();
        Cache::clear();

        App::build(
            array(
                'Utility' => array(
                    CakePlugin::path('Database') . 'Utility' . DS,
                    CakePlugin::path('Database') . 'Test' . DS . 'test_app' . DS . 'Utility' . DS
                )
            ),
            App::REGISTER
        );

        $this->Site = ClassRegistry::init(array('class' => 'Database.DatabaseSite', 'alias' => 'Site'));
        $this->User = ClassRegistry::init(array('class' => 'Database.DatabaseUser', 'alias' => 'User'));
    }

    /**
     * Method executed after each test
     */
    public function tearDown()
    {
        unset($this->User);
        unset($this->Site);
        parent::tearDown();
    }

    protected function _getDatabaseFormattableSettings(Model $model)
    {
        $result = array();

        $reflect = new ReflectionClass($model->Behaviors->DatabaseFormattable);
        $props = $reflect->getProperties(ReflectionProperty::IS_PROTECTED);
        foreach ($props as $prop) {
            if (in_array($prop->getName(), ['_cacheLoaded', '_typeByField', '_fieldsByType', '_regexes'])) {
                $prop->setAccessible(true);
                $result[$prop->getName()] = $prop->getValue($model->Behaviors->DatabaseFormattable)[$model->alias];
            }
        }

        return $result;
    }

    /**
     * Test de la méthode DatabaseFormattableBehavior::beforeValidate() du
     * plugin Database.
     *
     * @return void
     */
    public function testBeforeValidate()
    {
        $this->Site->Behaviors->attach('Database.DatabaseFormattable');

        foreach ($this->records as $record) {
            $this->Site->create($record['data']);
            $this->Site->validates();
            $result = $this->Site->data;
            $this->assertEquals($record['expected'], $result, var_export($result, true));
        }
    }

    public function testBeforeValidateWithNotRegexp()
    {
        $this->User = ClassRegistry::init(array('class' => 'Database.DatabaseUser', 'alias' => 'User'));
        $params = array(
            'Database.DatabaseDefaultFormatter' => array(
                'formatTrim' => array('NOT' => array('/password/', 'binary')),
            ),
        );
        $this->User->Behaviors->attach('Database.DatabaseFormattable', $params);

        $data = array(
            'User' => array(
                'password' => ' foo ',
                'password_confirm' => ' bar ',
            )
        );
        $this->User->create($data);
        $this->User->validates();
        $expected = array(
            'User' => array(
                'is_active' => '1',
                'password' => ' foo ',
                'password_confirm' => ' bar ',
            )
        );
        $this->assertEquals($expected, $this->User->data);
    }

    public function testSettingsSite()
    {
        $this->Site = ClassRegistry::init(array('class' => 'Database.DatabaseSite', 'alias' => 'Site'));
        $params = array(
            'Database.DatabaseDefaultFormatter' => array(
                'formatTrim' => array('NOT' => array('/published/', 'binary')),
            ),
        );
        $this->Site->Behaviors->attach('Database.DatabaseFormattable', $params);

        // Settings are lazy loaded
        $this->Site->create(array());
        $this->Site->validates();

        $settings = $this->_getDatabaseFormattableSettings($this->Site);
        $expected = array(
            '_cacheLoaded' => true,
            '_fieldsByType' => array(
                'integer' => array('id', 'user_id'),
                'string' => array('name'),
                'float' => array('price'),
                'boolean' => array('published'),
                'binary' => array('document'),
                'text' => array('description'),
                'date' => array('birthday'),
                'time' => array('birthtime'),
                'datetime' => array('created', 'updated'),
            ),
            '_typeByField' => array(
                'id' => 'integer',
                'name' => 'string',
                'user_id' => 'integer',
                'price' => 'float',
                'published' => 'boolean',
                'document' => 'binary',
                'description' => 'text',
                'birthday' => 'date',
                'birthtime' => 'time',
                'created' => 'datetime',
                'updated' => 'datetime',
            ),
            '_regexes' => array(
                'Database.DatabaseDefaultFormatter' => array(
                    'formatTrim' => '/(?<!\\w)Site(\\.|\\.[0-9]+\\.)(id|name|user_id|price|description|birthday|birthtime|created|updated)$/',
                    'formatNull' => '/(?<!\\w)Site(\\.|\\.[0-9]+\\.)(id|name|user_id|price|published|document|description|birthday|birthtime|created|updated)$/',
                    'formatNumeric' => '/(?<!\\w)Site(\\.|\\.[0-9]+\\.)(price|id|user_id)$/',
                    'formatSuffix' => '/(?<!\\w)Site(\\.|\\.[0-9]+\\.)(user_id)$/',
                ),
            ),
        );
        $this->assertEquals($expected, $settings);
    }

    public function testSettingsUser()
    {
        $params = array(
            'Database.DatabaseDefaultFormatter' => array(
                'formatTrim' => array('NOT' => array('/password/', 'binary')),
            ),
        );
        $this->User->Behaviors->attach('Database.DatabaseFormattable', $params);

        // Settings are lazy loaded
        $this->User->create(array());
        $this->User->validates();

        $settings = $this->_getDatabaseFormattableSettings($this->User);
        $expected = array(
            '_cacheLoaded' => true,
            '_fieldsByType' => array(
                'integer' => array('id', 'group_id'),
                'string' => array('username', 'password', 'salt', 'role', 'is_active'),
                'datetime' => array('created', 'modified', 'sent_email'),
            ),
            '_typeByField' => array(
                'id' => 'integer',
                'group_id' => 'integer',
                'username' => 'string',
                'password' => 'string',
                'salt' => 'string',
                'role' => 'string',
                'created' => 'datetime',
                'modified' => 'datetime',
                'is_active' => 'string',
                'sent_email' => 'datetime',
            ),
            '_regexes' => array(
                'Database.DatabaseDefaultFormatter' => array(
                    'formatTrim' => '/(?<!\\w)User(\\.|\\.[0-9]+\\.)(id|group_id|username|salt|role|created|modified|is_active|sent_email)$/',
                    'formatNull' => '/(?<!\\w)User(\\.|\\.[0-9]+\\.)(id|group_id|username|password|salt|role|created|modified|is_active|sent_email)$/',
                    'formatNumeric' => '/(?<!\\w)User(\\.|\\.[0-9]+\\.)(id|group_id)$/',
                    'formatSuffix' => '/(?<!\\w)User(\\.|\\.[0-9]+\\.)(group_id)$/',
                ),
            ),
        );
        $this->assertEquals($expected, $settings);
    }

    /**
     * Test de la méthode DatabaseFormattableBehavior::beforeSave() du
     * plugin Database.
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->Site->Behaviors->attach('Database.DatabaseFormattable');

        foreach ($this->records as $record) {
            $this->Site->create($record['data']);
            $this->Site->Behaviors->trigger('beforeSave', array($this->Site));
            $result = $this->Site->data;

            $this->assertEquals($record['expected'], $result, var_export($result, true));
        }
    }

    /**
     * Tet de la méthode DatabaseFormattableBehavior::doFormatting() lorsqu'une
     * autre classe de formattage est utilisée.
     */
    public function testOtherFormattingClass()
    {
        $config = array(
            'Database.DatabaseDefaultFormatter' => false,
            'Database.DatabaseTestFormatters' => array(
                'formatStar' => true,
            )
        );
        $this->Site->Behaviors->attach('Database.DatabaseFormattable', $config);

        $result = $this->Site->doFormatting(array('Site' => array('id' => 5, 'name' => 'FooBar')));
        $expected = array(
            'Site' => array(
                'id' => '*',
                'name' => '******'
            )
        );
        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode DatabaseFormattableBehavior::doFormatting() lorsqu'une
     * exception est renvoyée.
     *
     * @expectedException MissingUtilityException
     *
     * @return void
     */
    public function testDoFormattingException()
    {
        $config = array(
            'Database.DatabaseDefaultFormatter' => false,
            'Database.DatabaseTestFormatters' => false,
            'InexistantFormatter' => array(
                'formatNull' => true,
            )
        );

        $this->Site->Behaviors->attach('Database.DatabaseFormattable', $config);
        $this->Site->doFormatting(array('Site' => array('id' => 5)));
    }
}

?>
