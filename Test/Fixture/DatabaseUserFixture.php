<?php
/**
 * Short description for file.
 *
 * PHP version 5.3
 *
 * @package Database
 * @subpackage Test.Fixture
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */

/**
 * Short description for class.
 *
 * @package Database
 * @subpackage Test.Fixture
 */
class DatabaseUserFixture extends CakeTestFixture
{

    /**
     * name property
     *
     * @var string 'DatabaseUser'
     * @access public
     */
    public $name = 'DatabaseUser';

    /**
     * fields property
     *
     * @var array
     * @access public
     */
    public $fields = array(
        'id' => array( 'type' => 'integer', 'key' => 'primary' ),
        'group_id' => array( 'type' => 'integer', 'null' => false ),
        'username' => array( 'type' => 'string', 'length' => 255, 'null' => false ),
        'password' => array( 'type' => 'string', 'length' => 255, 'null' => false ),
        'salt' => array( 'type' => 'string', 'length' => 40, 'null' => false ),
        'role' => array( 'type' => 'string', 'length' => 10, 'null' => false ),
        'created' => 'datetime',
        'modified' => 'datetime',
        'is_active' => array( 'type' => 'string', 'length' => 1, 'null' => false, 'default' => '1' ),
        'sent_email' => 'datetime',
        'indexes' => array(
            'users_username_idx' => array(
                'column' => array( 'username' ),
                'unique' => 1
            ),
            'users_group_id_idx' => array(
                'column' => array( 'group_id' )
            ),
        )
    );
}
?>
