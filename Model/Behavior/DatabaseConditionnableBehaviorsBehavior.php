<?php
/**
 * Code source de la classe DatabaseConditionnableBehaviorsBehavior.
 *
 * PHP 5.3
 *
 * @package Database
 * @subpackage Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
// @codeCoverageIgnoreStart
App::import( 'Model', 'Model' );
// @codeCoverageIgnoreEnd

/**
 * La classe DatabaseConditionnableBehaviorsBehavior permet de charger des behviors
 * de manière conditionnelle.
 * 
 * Les variables disponibles sont exportées soit dans la méthode
 * getDatabaseConditionnableBehaviorsParams du modèle, soit dans celle du behavior.
 *
 * @package Database
 * @subpackage Model.Behavior
 */
class DatabaseConditionnableBehaviorsBehavior extends ModelBehavior
{
    /**
     * Configuration du behavior pour chacun des alias des modèles.
     *
     * @var array
     * @see Model::$alias
     */
    public $settings = [];

    /**
     * Configuration du behavior et chargement des behaviors de manière conditionnée.
     *
     * @param Model $Model Le modèle qui utilise ce behavior
     * @param array $config La configuration à appliquer
     */
    public function setup( Model $Model, $config = [] )
    {
        $this->settings[$Model->alias] = (array)Hash::normalize($config);
        $behaviors = $this->_behaviors($Model, $this->settings[$Model->alias]);
        $this->_load($Model, $behaviors);
    }

    /**
     * Retourne les variables pouvant être utilisées dans les conditions:
     * $configure, $name, $alias, $useTable, $dataSource.
     * 
     * @param Model $Model
     * @return array
     */
    public function getDatabaseConditionnableBehaviorsParams(Model $Model)
    {
        return [
            'configure' => (array)Configure::read(),
            'name' => $Model->name,
            'alias' => $Model->alias,
            'useTable' => $Model->useTable,
            'dataSource' => get_class($Model->getDataSource())
        ];
    }

    /**
     * Evalue (eval) du code en fonction des paramètres.
     * 
     * @param type $code Le code à évaluer
     * @param array $params Les paramètres utilisables dans l'évaluation
     * @return mixed
     */
    protected function _eval($code, array $params)
    {
        extract($params);
        return eval("return ( {$code} );");
    }

    /**
     * Retourne la liste des behaviors à charger en fonction de la configuration.
     * 
     * @param Model $Model
     * @param array $settings La configuration
     * @return array
     */
    protected function _behaviors(Model $Model, array $settings)
    {
        $result = [];
        // @todo: pouvoir surcharger dans les modèles ou dans d'autres behaviors
        if (method_exists($Model, 'getDatabaseConditionnableBehaviorsParams')) {
            $params = $Model->getDatabaseConditionnableBehaviorsParams();
        } else {
            $params = $this->getDatabaseConditionnableBehaviorsParams($Model);
        }

        foreach ($settings as $condition => $behaviors) {
            if ($this->_eval($condition, $params) === true) {
                $result = Hash::merge($result, Hash::normalize($behaviors));
            }
        }
        return $result;
    }

    /**
     * Chargement de la liste des behaviors. Retourne vrai si tout s'est bien
     * passé.
     * 
     * @param Model $Model
     * @param array $behaviors La liste des behaviors
     * @return bool
     */
    protected function _load(Model $Model, array $behaviors)
    {
        $result = true;
        foreach (Hash::normalize($behaviors) as $behavior => $config) {
            $result = $Model->Behaviors->load($behavior, $config) && $result;
        }
        return $result;
    }
}
