<?php
/**
 * Code source de la classe DatabaseEnumerableBehavior.
 *
 * PHP 5.3
 *
 * @package Database
 * @subpackage Model.Behavior
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
// @codeCoverageIgnoreStart
App::import( 'Model', 'Model' );
require_once CakePlugin::path( 'Database' ).'Config'.DS.'bootstrap.php';
// @codeCoverageIgnoreEnd

/**
 * La classe DatabaseEnumerableBehavior ...
 * 
 * @package Database
 * @subpackage Model.Behavior
 */
class DatabaseEnumerableBehavior extends ModelBehavior
{
    /**
     * Contains configuration settings for use with individual model objects.  This
     * is used because if multiple models use this Behavior, each will use the same
     * object instance.  Individual model settings should be stored as an
     * associative array, keyed off of the model name.
     *
     * @var array
     * @see Model::$alias
     */
    public $settings = [];

    /**
     * Paramètres par défaut.
     *
     * La clé "domain" sera déduite de l'alias du modèle.
     * La clé "translate" servira pour la traduction: il s'agit du nom de la
     * fonction de traduction et des éventuels paramètres précédant la chaîne à
     * traduire. On peut se servir de "%domain%" pour référencer le domaine.
     *
     * @var array
     */
    public $defaultSettings = [
        'domain' => null,
        'translate' => ['__d', '%domain%']
    ];

    /**
     * Cache "live" pour les enums des modèles.
     *
     * @var array
     */
    protected $_enumsCache = [];

    /**
     * Configuration du behavior.
     *
     * @param Model $Model Le modèle qui utilise ce behavior
     * @param array $config La configuration à appliquer
     */
    public function setup( Model $Model, $config = [] )
    {
        $config += $this->defaultSettings;

        if (isset($this->settings[$Model->alias]) === false) {
            $this->settings[$Model->alias] = [];
        }

        $this->settings[$Model->alias] = Hash::merge(
            $this->settings[$Model->alias],
            (array) Hash::normalize($config)
        );

        if ($this->settings[$Model->alias]['domain'] === null) {
            $this->settings[$Model->alias]['domain'] = Inflector::underscore($Model->alias);
        }
        
        if ($this->settings[$Model->alias]['translate'] !== false) {
            $this->settings[$Model->alias]['translate'] = str_replace(
                '%domain%',
                $this->settings[$Model->alias]['domain'],
                $this->settings[$Model->alias]['translate']
            );
        }
    }

    /**
     * Traduction des valeurs de l'enum, en utilisant la fonction définie dans 
     * la configuration et éventuellement le domaine.
     * 
     * @param Model $Model
     * @param string $fieldName
     * @param array $values
     * @return array
     */
    protected function _translate(Model $Model, $fieldName, array $values)
    {
        $result = [];
        $fieldNameUpper = strtoupper($fieldName);
        if ($this->settings[$Model->alias]['translate'] !== false) {
            $translate = $this->settings[$Model->alias]['translate'][0];
            $params = array_slice($this->settings[$Model->alias]['translate'], 1);
        } else {
            $translate = false;
        }

        foreach ($values as $value) {
            if ($translate === false) {
                $result[$value] = $value;
            } else {
                $result[$value] = call_user_func_array(
                    $translate,
                    array_merge($params, ["ENUM::{$fieldNameUpper}::{$value}"])
                );                
            }
        }
        
        return $result;
    }

    /**
     * Retourne un array dont la première clé est l'alias du modèle et ensuite,
     * pour chacun des champs ayant des règles de validation inList, le nom du
     * champ ainsi qu'un array contenant en clé la valeur et en valeur la traduction.
     * 
     * @param Model $Model
     * @return array
     */
    public function enums(Model $Model)
    {
        $cacheKey = $Model->useDbConfig . '_' . __CLASS__ . '_' . __FUNCTION__ . '_' . $Model->alias;

        // Dans le cache "live" ?
        if (false === isset($this->_enumsCache[$cacheKey])) {
            $this->_enumsCache[$cacheKey] = Cache::read($cacheKey);

            // Dans le cache CakePHP ?
            if (false === $this->_enumsCache[$cacheKey]) {
                $this->_enumsCache[$cacheKey] = [];

                // D'autres champs avec la règle inList ?
                foreach ($Model->validate as $field => $validate) {
                    foreach ($validate as $ruleName => $rule) {
                        $hasInlistRule = isset($rule['rule'][0]) === true && strtolower($rule['rule'][0]) === 'inlist';
                        if ($hasInlistRule && isset($this->_enumsCache[$cacheKey][$Model->alias][$field]) === false) {
                            $list = $this->_translate($Model, $field, $rule['rule'][1]);
                            $this->_enumsCache[$cacheKey][$Model->alias][$field] = $list;
                        }
                    }
                }

                Cache::write($cacheKey, $this->_enumsCache[$cacheKey]);
            }
        }
        
        return $this->_enumsCache[$cacheKey];
    }

    /**
     * Retourne, pour un champ donné, un array contenant en clé la valeur et en
     * valeur la traduction.
     * 
     * @param Model $Model
     * @param string $fieldName
     * @return array
     */
    public function enum(Model $Model, $fieldName)
    {
        return (array)Hash::get($this->enums($Model), "{$Model->alias}.{$fieldName}");
    }
}
