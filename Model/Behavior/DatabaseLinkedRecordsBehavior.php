<?php
App::uses( 'ModelBehavior', 'Model' );

class DatabaseLinkedRecordsBehavior extends ModelBehavior
{
    /**
     * La masque du format des champs virtuels.
     *
     * @var string
     */
    public $fieldNameFormat = 'has_%s';

    /**
     * Retourne le nom du champ virtuel qui sera utilisé pour un modèle lié
     * donné.
     *
     * @param Model $Model
     * @param string $linkedModelName
     * @return string
     */
    public function linkedRecordsVirtualFieldName(Model $Model, $linkedModelName)
    {
        $linkedModelName = Inflector::classify($linkedModelName);
        $tableName = Inflector::tableize($linkedModelName);
        return sprintf($this->fieldNameFormat, Inflector::singularize($tableName));
    }

    /**
     * Retourne la liste des alias des modèles directement liés à notre modèle,
     * via les relations hasOne, hasMany et hasAndBelongsToMany (with).
     * 
     * @param Model $Model
     * @return array
     */
    public function linkedRecordsModelsAliases(Model $Model)
    {
        $result = [];

        foreach ($Model->hasAndBelongsToMany as $alias => $params) {
            $result[] = $params['with'];
        }

        foreach (array_keys($Model->hasMany) as $alias) {
            $result[] = $alias;
        }

        foreach (array_keys($Model->hasOne) as $alias) {
            $result[] = $alias;
        }

        return $result;
    }

    /**
     * Complète la partie fields d'un querydata avec les champs virtuels sur
     * les enregistrements liés passés en paramètre.
     */
    public function linkedRecordsCompleteQuerydata(Model $Model, array $querydata, $summary = false, $links = null)
    {
        $querydata['fields'] = (array)$querydata['fields'];
        if ($links === null) {
            $links = $this->linkedRecordsModelsAliases($Model);
        }
        $links = Hash::normalize((array)$links);
        $fields = [];

        foreach ($links as $linkedModelName => $linkQuerydata) {
            $virtualFieldName = $this->linkedRecordsVirtualFieldName($Model, $linkedModelName);
            $virtualField = $this->linkedRecordsVirtualField($Model, $linkedModelName, $linkQuerydata);

            $fields["{$Model->alias}__{$virtualFieldName}"] = $virtualField;
        }

        if ($summary === false) {
            foreach ($fields as $alias => $sql) {
                $querydata['fields'][] = "( {$sql} ) AS \"{$alias}\"";
            }
        } else {
            if (is_string($summary) === false) {
                $summary = sprintf($this->fieldNameFormat, 'linked_records');
            }
            if (empty($fields) === true) {
                $sql = 'FALSE';
            } else {
                $sql = implode(' OR ', $fields);
            }
            $querydata['fields'][] = "({$sql}) AS \"{$Model->alias}__{$summary}\"";
        }

        return $querydata;
    }

    /**
     * Complète l'attribut virtualFields du modèle avec les champs virtuels
     * sur les enregistrements liés passés en paramètre.
     *
     * @param Model $Model
     * @param array|string $links
     */
    public function linkedRecordsLoadVirtualFields(Model $Model, $links)
    {
        $links = Hash::normalize((array)$links);

        foreach ($links as $linkedModelName => $linkQuerydata) {
            $virtualFieldName = $this->linkedRecordsVirtualFieldName($Model, $linkedModelName);
            $virtualField = $this->linkedRecordsVirtualField($Model, $linkedModelName, $linkQuerydata);

            $Model->virtualFields[$virtualFieldName] = $virtualField;
        }
    }

    /**
     * Retourne la requête SQL à utiliser en tant que champ virtuel pour un
     * modèle lié donné. Des conditions et des jointures sont possibles dans
     * le querydata.
     *
     * @param Model $Model
     * @param string $modelName
     * @param array $querydata
     * @param boolean $aliasJoins
     * @return string
     */
    public function linkedRecordsVirtualField(Model $Model, $modelName, $querydata = null, $aliasJoins = true)
    {
        $tableName = Inflector::tableize($modelName);
        $replacements = [$modelName => $tableName];
        $conditions = (array)Hash::get((array)$querydata, 'conditions');
        $joins = (array)Hash::get((array)$querydata, 'joins');

        $join = $Model->join($modelName);
        $conditions[] = $join['conditions'];

        $qdSubquery = [
            'alias' => $tableName,
            'fields' => ["{$tableName}.{$Model->{$modelName}->primaryKey}"],
            'conditions' => $conditions,
            'contain' => false,
            'joins' => [],
            'limit' => 1
        ];

        if (!empty($joins) && $aliasJoins) {
            foreach ($joins as $join) {
                $tableName = Inflector::tableize($join['alias']);
                $replacements[$join['alias']] = $tableName;
                $qdSubquery['joins'][] = $join;
            }
        }

        $qdSubquery = alias_querydata($qdSubquery, $replacements);

        $subquery = $Model->{$modelName}->sql($qdSubquery);

        // Permet de remplacer les mentions à {$__cakeID__$} dans la sous-requête
        $Dbo = $Model->getDataSource();
        $cakeId = alias_querydata(
                ["{$Dbo->startQuote}{$Model->alias}{$Dbo->endQuote}.{$Dbo->startQuote}{$Model->primaryKey}{$Dbo->endQuote}"], $replacements
        );
        $subquery = str_replace('{$__cakeID__$}', $cakeId[0], $subquery);

        return preg_replace(
                '/^EXISTS\(SELECT +[^ ]+ +AS +[^ ]+ +FROM /', 'EXISTS(SELECT * FROM ', "EXISTS({$subquery})"
        );
    }

}
